# Whiteboards

Whiteboards is an experimentation area for GNOME design. It is intended as a 
place where people can safely share and discuss their ideas.

Feel free to use issues to post mockups or ideas. Those with the necessary 
access can also push to the repository.
